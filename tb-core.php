<?php
/**
 * Plugin Name: TB Core
 * Plugin URI: http://www.themesbros.com
 * Description: Shortcodes and functions for ThemesBros themes.
 * Author: ThemesBros
 * AuthorURI: http://www.themesbros.com
 * Version: 1.0.0
 *
 * @package     TB Core
 * @author      ThemesBros.com
 * @copyright   Copyright (c) 2011-2017, ThemesBros
 */

/* If this file is called directly, abort. */
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Plugin loader class.
 *
 * @since 1.0.0
 */
class TB_Core {

	/**
	 * Plugin directory.
	 *
	 * @since   1.0.0
	 * @access  protected
	 * @var     string
	 */
	protected $plugin_dir;

	/**
	 * Sets up needed actions/filters for the plugin to initialize.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'includes' ) );
		add_action( 'plugins_loaded', array( $this, 'i18n' ), 3 );
	}

	/**
	 * Loads the translation files.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function i18n() {
		load_plugin_textdomain( 'tb-core', false, $this->plugin_dir . 'languages' );
	}

	/**
	 * Loads the initial files needed by the plugin.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function includes() {
		require_once plugin_dir_path( __FILE__ ) . '/inc/constants.php';
		require_once plugin_dir_path( __FILE__ ) . '/inc/helpers.php';
		require_once plugin_dir_path( __FILE__ ) . '/inc/hooks.php';
		require_once plugin_dir_path( __FILE__ ) . '/inc/class-tb-shortcodes.php';
	}

}

// Run!
new TB_Core();
