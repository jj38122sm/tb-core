<?php
/**
 * Shortcodes for users.
 *
 * @package     TB Core
 * @subpackage  Shortcodes
 * @author      ThemesBros.com
 * @copyright   Copyright (c) 2011-2017, ThemesBros
 */

/**
 * Shortcode loader class.
 *
 * @since 1.0.0
 */
class TB_Shortcodes {

	/**
	 * Sets up needed actions/filters for the plugin to initialize.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function __construct() {
		// Add script.
		add_filter( 'mce_external_plugins', array( $this, 'enqueue' ) );

		// Add button.
		add_filter( 'mce_buttons', array( $this, 'add_buttons' ) );
	}

	/**
	 * Adds script file to mce scripts.
	 *
	 * @since  1.0.0
	 * @param  array $plugin_array An array of all plugins.
	 * @return array
	 */
	public function enqueue( $plugin_array ) {
		// Use minified files if SCRIPT_DEBUG is off.
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		$plugin_array['tb_core_shortcodes'] = TBC_JS . "shortcodes{$suffix}.js";
		return $plugin_array;
	}

	/**
	 * Adds button to mce buttons.
	 *
	 * @since  1.0.0
	 * @param  array $buttons Existing buttons.
	 * @return array
	 */
	public function add_buttons( $buttons ) {
		array_push( $buttons, 'tb_core_shortcodes' );
		return $buttons;
	}

}

// Run!
new TB_Shortcodes();
