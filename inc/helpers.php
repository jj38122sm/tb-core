<?php
/**
 * Helper functions.
 *
 * @package     TB Core
 * @subpackage  Helpers
 * @author      ThemesBros.com
 * @copyright   Copyright (c) 2011-2017, ThemesBros
 */

/**
 * Post views counter.
 *
 * @since  1.0.0
 * @access public
 * @return int
 */
function tb_core_get_post_views() {
	$views = get_post_meta( get_the_ID(), tb_core_post_views_key(), true );
	return apply_filters( 'tb_core_views_forgery', $views );
}

/**
 * Defines & returns post meta key to store post views.
 *
 * @since  1.0.0
 * @access public
 * @return string
 */
function tb_core_post_views_key() {
	return apply_filters( 'tb_core_post_views_key', 'tb_core_post_views' );
}

/**
 * Updates post views number.
 *
 * @since 1.0.0
 * @access public
 * @return void
 */
function tb_core_set_post_views() {
	if ( is_singular() ) {
		$id    = get_the_ID();
		$views = absint( get_post_meta( $id, tb_core_post_views_key(), true ) ) + 1;
		update_post_meta( $id, tb_core_post_views_key(), $views );
	}
}

/**
 * Sets and returns number of post views.
 *
 * @since  1.0.0
 * @access public
 * @return int
 */
function tb_core_post_views() {
	tb_core_set_post_views();
	return tb_core_get_post_views();
}
