<?php
/**
 * Filters & actions.
 *
 * @package     TB Core
 * @subpackage  Hooks
 * @author      ThemesBros.com
 * @copyright   Copyright (c) 2011-2017, ThemesBros
 */

// Add more links to social profiles.
add_filter( 'user_contactmethods', 'tb_core_author_add_profiles', 10, 1 );

/**
 * Adds additional fields for social profiles.
 *
 * @since  1.0.0
 * @access public
 * @param  array $profiles Existing profile fields.
 * @return array
 */
function tb_core_author_add_profiles( $profiles ) {

	$profiles['facebook_profile'] = esc_html__( 'Facebook URL', 'tb-core' );
	$profiles['twitter_profile']  = esc_html__( 'Twitter URL', 'tb-core' );
	$profiles['google_profile']   = esc_html__( 'Google URL', 'tb-core' );
	$profiles['linkedin_profile'] = esc_html__( 'Linkedin URL', 'tb-core' );
	$profiles['youtube_profile']  = esc_html__( 'Youtube URL', 'tb-core' );

	return $profiles;
}
