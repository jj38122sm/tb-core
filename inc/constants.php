<?php
/**
 * Constants.
 *
 * @package     TB Core
 * @subpackage  Constants
 * @author      ThemesBros.com
 * @copyright   Copyright (c) 2011-2017, ThemesBros
 */

define( 'TBC_DIR', trailingslashit( dirname( plugin_dir_path( __FILE__ ) ) ) );
define( 'TBC_URI', trailingslashit( dirname( plugin_dir_url( __FILE__ ) ) ) );
define( 'TBC_CSS', TBC_URI . 'assets/css/' );
define( 'TBC_JS', TBC_URI . 'assets/js/' );
