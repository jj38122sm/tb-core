/**
 * Gulpfile.
 *
 * @author Sinisa Nikolic (sin2384)
 * @version 1.0.0
 */

// Project info:
var themeName  = 'TB Core';
var pluginTextDomain = 'tb-core';
var proxyURL = 'http://localhost/theme/starter/';

// Browsers you care about for autoprefixing.
// Browserlist https://github.com/ai/browserslist
const AUTOPREFIXER_BROWSERS = [
    'last 2 version',
    '> 1%',
    'ie >= 9',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
  ];

const gulp 	     = require('gulp'),
	browserSync  = require('browser-sync').create(),
	autoprefixer = require('gulp-autoprefixer'),
	concat       = require('gulp-concat'),
	cssmin       = require('gulp-cssmin'),
	imagemin     = require('gulp-imagemin'),
	rename       = require('gulp-rename'),
	rtlcss       = require('gulp-rtlcss'),
	sass         = require('gulp-sass'),
	sort         = require('gulp-sort'),
	uglify       = require('gulp-uglify'),
	copy         = require('gulp-copy'),
	wppot        = require('gulp-wp-pot'),
    zip          = require('gulp-zip'),
    replace      = require('gulp-replace'),
	mmq          = require('gulp-merge-media-queries'),
    exec         = require('child_process').exec;

gulp.task('sass', function() {
    gulp.src('assets/scss/**/*.scss')
        .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
        .pipe(mmq())
        .pipe(autoprefixer( AUTOPREFIXER_BROWSERS ))
        .pipe(gulp.dest('assets/css/'))
        .pipe(browserSync.stream());
});

gulp.task('styles', function() {
    gulp.src(['assets/**/*.css', '!assets/**/*.min.css', 'style.css', '!style.min.css'])
        .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./assets/'))
});

gulp.task('rtlcss', function() {
    gulp.src(['style.css'])
        .pipe(rtlcss())
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(gulp.dest('assets/css'))
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('assets/css'))
});

gulp.task('scripts', function() {
    gulp.src(['assets/**/*.js', '!**/*.min.js'])
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(function(file) {
            return file.base;
        }))
});

gulp.task('imagemin', function() {
    gulp.src('assets/images/**')
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({
                plugins: [
                    { removeViewBox: true },
                    { cleanupIDs: false }
                ]
            })
        ]))
        .pipe(gulp.dest('assets/images/'))
});

gulp.task('pot', function() {
    gulp.src('**/*.php')
        .pipe(wppot({
            domain: pluginTextDomain,
            package: themeName,
            bugReport: 'http://www.themesbros.com/contact/',
            team: 'ThemesBros.com <support@themebros.com>',
            headers: {
                Language: 'en_US',
                'X-Poedit-SearchPath-0': '..'
            }
        }))
        .pipe(gulp.dest('languages/'+pluginTextDomain+'.pot'));
});

gulp.task('serve', function() {

    browserSync.init({
        proxy: proxyURL,
        notify: false,
        injectChanges: true,
        ghostMode: {
            forms: false,
        }
    });

    gulp.watch(['assets/scss/**/*.scss'], ['sass']);

    gulp.watch('style.css', function() {
        gulp.src('style.css')
            .pipe(browserSync.stream())
            .pipe(cssmin())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('./'))
    });

    gulp.watch("**/*.php").on('change', browserSync.reload);
    gulp.watch("**/*.js").on('change', browserSync.reload);
});

gulp.task('zip', () => {
    return gulp.src(['**/*', '!*sublime*', '!{.git,.git/**,scss,scss/**,node_modules,node_modules/**}', '!gulpfile.js', '!*.json'])
        .pipe(zip( pluginTextDomain + '.zip'))
        .pipe(gulp.dest('/home/sin/Desktop/'));
});

gulp.task('build', ['sass', 'styles', 'rtlcss', 'scripts', 'imagemin', 'pot']);
