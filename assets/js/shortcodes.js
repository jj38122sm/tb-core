(function() {
    var tinymce = window.tinymce || {};
	tinymce.PluginManager.add(
		'tb_core_shortcodes',
		function(editor, url) {
			editor.addButton(
				'tb_core_shortcodes',
				{
					icon: true,
					image: url.replace( 'js', 'images' ) + '/shortcodes-icon.png',
					type: 'menubutton',
					menu: [
					{
						text: 'Dropcaps',
						onclick: function() {
							editor.windowManager.open(
								{
									title: 'Dropcaps',
									body: [
									{ type: 'textbox', name: 'letter', label: 'Letter' },
									{
										type   : 'listbox',
										name   : 'type',
										label  : 'Type',
										values : [
										{ text: 'Regular', value: 'dropcap' },
										{ text: 'Square', value: 'dropcap dc-square' }
										],
										value : 'dropcap'
									}
									],
									onsubmit: function(e) {
										editor.insertContent( '<span class="' + e.data.type + '">' + e.data.letter + '</span>' );
									}
								}
							);
						}
					},
					{
						text: 'Highlight',
						onclick: function() {
							editor.windowManager.open(
								{
									title: 'Highlight',
									body: [
									{ type: 'textbox', name: 'text', label: 'Text' },
									{
										type   : 'colorpicker',
										name   : 'bg',
										label  : 'Background Color'
									},
									{
										type   : 'colorpicker',
										name   : 'color',
										label  : 'Text Color'
									}
									],
									onsubmit: function(e) {
										editor.insertContent( '<span style="background-color:' + e.data.bg + '; color: ' + e.data.color + ';">' + e.data.text + '</span>' );
									}
								}
							);
						}
					}
					]
				}
			);

		}
	);
})();
